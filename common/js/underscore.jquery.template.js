(function(){
  var cache = {};
  $.template = function(name, data){
      var template = cache[name];
      if (!template){
            template = $('script.' + name).html();
            cache[name] = template;
      }
      return _.template(template, data || {});
  };

  $.is_template = function(name){
  if(!cache[name]) return false;
  else return true;
  };
})();