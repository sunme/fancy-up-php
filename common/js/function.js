//@ return String (device name)
function check_mobile_device(){
    var mobileKeyWords = new Array('iPhone', 'iPod', 'BlackBerry', 'Android', 'Windows CE', 'LG', 'MOT', 'SAMSUNG', 'SonyEricsson');//iPad
    var device_name = '';
    for (var word in mobileKeyWords){
        if (navigator.userAgent.match(mobileKeyWords[word]) != null){
            device_name=mobileKeyWords[word];
            break;
        }
    }
return device_name;
}

function check_msie() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("chrome") != -1) return '0';
    if (agt.indexOf("opera") != -1) return '0';
    if (agt.indexOf("staroffice") != -1) return '0';
    if (agt.indexOf("webtv") != -1) return '0';
    if (agt.indexOf("beonex") != -1) return '0';
    if (agt.indexOf("chimera") != -1) return '0';
    if (agt.indexOf("netpositive") != -1) return '0';
    if (agt.indexOf("phoenix") != -1) return '0';
    if (agt.indexOf("firefox") != -1) return '0';
    if (agt.indexOf("safari") != -1) return '0';
    if (agt.indexOf("skipstone") != -1) return '0';
    if (agt.indexOf("msie") != -1) return '1';
    if (agt.indexOf("netscape") != -1) return '0';
    if (agt.indexOf("mozilla/5.0") != -1) return '1';
}

function parseUrl(url){
    var a=document.createElement('a');
    a.href=url;
    return a;
}

function loadCSS(href) {
     var cssLink = $("<link rel='stylesheet' type='text/css' href='"+href+"'>");
     $("head").append(cssLink);
}

/**
 * function to load a given js file
 */
function loadJS(src) {
     var jsLink = $("<script type='text/javascript' src='"+src+"'>");
     $("head").append(jsLink);
 };

/**
 *url을 json 데이터로 변환
 * @param {Object} url
 */
function convertUrlToJson(url) {
    var hash;
    var json = {};
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        json[hash[0]] = hash[1];
    }
    return json;
}

// 휴대전화 자동하이픈 넣기
function phonNumStr( str ){
    var RegNotNum  = /[^0-9]/g;
    var RegPhonNum = "";
    var DataForm   = "";

    // return blank
    if( str == "" || str == null ) return "";

    // delete not number ㅋ
    str = str.replace(RegNotNum,'');

    if( str.length < 4 ){
        if(str.length==1){
            var intv = parseInt(str.substring(0,1));
            if(intv>0){str = "";}
        }else if(str.length==2){
            if(str !='01'){
                return "01";
            }
        }else if(str.length==3){
            var intv = parseInt(str.substring(2,3));
            if(intv==0 || intv==1){
                return str;
            }else if(intv<6 || intv>9){
                return "01";
            }
        }
        return str;
    }

    if( str.length > 3 && str.length < 7 ) {
        DataForm = "$1-$2";
        RegPhonNum = /([0-9]{3})([0-9]+)/;
    } else if(str.length == 7 ) {
        DataForm = "$1-$2";
        RegPhonNum = /([0-9]{3})([0-9]{4})/;
    } else {
        DataForm = "$1-$2-$3";
        RegPhonNum = /([0-9]{3})([0-9]{4})([0-9]+)/;
    }

    while( RegPhonNum.test(str) ) {
        str = str.replace(RegPhonNum, DataForm);
    }
    return str;
}

function telNumStr( str ){
    var RegNotNum  = /[^0-9]/g;
    var RegPhonNum = "";
    var DataForm   = "";

    // return blank
    if( str == "" || str == null ) return "";

    // delete not number ㅋ
    str = str.replace(RegNotNum,'');

    if( str.length < 4 ){
        if(str.length==1){
            var intv = parseInt(str.substring(0,1));
            if(intv>0){str = "";}
        }else if(str.length==2){
        	intv = parseInt(str.substring(1,2));
            if(intv > 1 && intv < 7){
                return str;
            }
            else {
            	return "0";
            }
        }else if(str.length==3){
            intv = parseInt(str.substring(2,3));
            var intv_bak = str.substr(0,2);
            if(intv == 02){
                return str;
            }
            else if(intv < 2 || intv > 5){
            	return intv_bak;
            }
        }
        return str;
    }

    if( str.length > 2 && str.length < 6 ) {
        DataForm = "$1-$2";
        RegPhonNum = /([0-9]{2})([0-9]{3})/;
    } else if(str.length > 5 && str.length < 9 ) {
        DataForm = "$1-$2-$3";
        RegPhonNum = /([0-9]{2})([0-9]{3})([0-9]+)/;
    } else if(str.length == 9 ){
        DataForm = "$1-$2-$3";
        RegPhonNum = /([0-9]{2})([0-9]{3})([0-9]+)/;
    } else {
        DataForm = "$1-$2-$3";
        RegPhonNum = /([0-9]{3})([0-9]{3})([0-9]+)/;
    }

    while( RegPhonNum.test(str) ) {
        str = str.replace(RegPhonNum, DataForm);
    }
    return str;
}

function dateNumStr( str ){
    var RegNotNum  = /[^0-9]/g;
    var RegPhonNum = "";
    var DataForm   = "";

    // return blank
    if( str == "" || str == null ) return "";

    // delete not number ㅋ
    str = str.replace('-','');
    str = str.replace(RegNotNum,'');

    if( str.length < 5 ){
        if(str.length==1){
            var intv = parseInt(str.substring(0,1));
            if(intv<1 || intv>2){str = "";}
        }
        return str;
    }

    if( str.length > 4 && str.length < 7 ) {
        DataForm = "$1-$2";
        RegPhonNum = /([0-9]{4})([0-9]+)/;
    } else if(str.length == 7 ) {
        DataForm = "$1-$2";
        RegPhonNum = /([0-9]{4})([0-9]{2})/;
    } else {
        DataForm = "$1-$2-$3";
        RegPhonNum = /([0-9]{4})([0-9]{2})([0-9]+)/;
    }

    while( RegPhonNum.test(str) ) {
        var len = str.length;
        if(len==5){
            var intv = parseInt(str.substring(4,5));
            if(intv>1){str = str.substring(0,4)+'-0'+intv; break;}
        }else if(len==6){
            var intv = parseInt(str.substring(5,6));
            var intv2 = parseInt(str.substring(4,5));
            if(intv2==1){
                if(intv>2){str= str.substring(0,5); break;}
            }
       }else if(len==7){
            var intv = parseInt(str.substring(6,7));
            if(intv>3){str= str.substring(0,4)+'-'+str.substring(4,6)+'-0'+intv; break;}
       }else if(len==8){
            var intv = parseInt(str.substring(6,7));
            if(intv==3){
                var intv2 = parseInt(str.substring(7,8));
                if(intv2>1){
                    str= str.substring(0,7); break;
                }
            }
       }
       str = str.replace(RegPhonNum, DataForm);
    }
    return str;
}