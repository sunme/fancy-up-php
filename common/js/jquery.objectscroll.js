/**
 * 객체를 포함한 컨테이너 스크롤 처리
 * @param  {[obj]} $ 컨테이너 오브젝트
 * @return {[void]}  undefined
 */
jQuery(function($)
{
	// setModalScroll
	// // 메뉴(서브메뉴) 스크롤
	$.fn.setMenuScroll = function(params) {

	    if (params.mid != '0000') {

	        // 메인메뉴 스크롤
	        var topMenu = $(this);
	        if (topMenu !== null) {
		        var topItem = $(params.topItem);
		        var topLeft = Math.ceil(topItem.position().left);
		        var topRight = Math.ceil(parseFloat(topItem.position().left + topItem.width()));
		        if (topRight >= topMenu.width()) {
		            topMenu.css({'overflow-x':'hidden'}).scrollLeft(topLeft).css({'overflow-x':'scroll'});
		        }
	        }

	        var subMenu = $(params.subMenu);
	        if (subMenu.length == 1) {
	        	// 서브메뉴 스크롤
	        	subMenu.show();
	            document.body.style.paddingTop = params.offsetY+'px';

	            var subItem = $(params.subItem);
	            var subLeft = Math.ceil(subItem.position().left);
	            var subRight = Math.ceil(parseFloat(subItem.position().left + subItem.width()));
	            if (subRight >= subMenu.width()) {
	                subMenu.css({'overflow-x':'hidden'}).scrollLeft(subRight).css({'overflow-x':'scroll'});
	            }
	        }
	    }
	}
});

function menuScroll(params) {

	if (check_mobile_device() != '') {
// alert('topMenu : ' + params.topMenu );
		$(params.topMenu).setMenuScroll({
			mid:params.mid,
	        topItem:'#hmenu_'+ params.mid.substr(0,2)+'00',		// #hmenu_2000
	        subMenu:params.subMenu,
	        subItem:'#menu_'+ params.mid,			// #menu_2001
	        offsetY:params.offsetY
	    });
	}
}
