jQuery(function($)
{
    $.fn.fusionAjaxRecord= function(params,successCallback, failCallback, logsCallback)
    {
        var targetID  = $(this);
        var callbacks = $.Callbacks();
        var callFunction =successCallback;
        var failFunction = failCallback;
        var logsFunction = logsCallback;

        var param = $.extend({
            type : 'post',
            filename : null,
            cache : false,
            dataType : 'text',
            crossDomain:true,
            data : ''
        },params||{});

        $.ajax( {
            type: param.type,
            dataType: param.dataType,
            // crossDomain: param.crossDomain,
            cache: param.cache,
            url: param.filename,
            data: param.data
        })
        .done(function(json)
        {
            // logs 함수가 있을 경우에만
            if(logsFunction !=null){
                callbacks.add(logsFunction);
                callbacks.fire(json);
            }else{
                if(param.dataType =='text'){ json = $.parseJSON(json); }
                if(typeof(json.result) != 'undefined')
                {
                    if(json.result == 'false'){
                        if(failFunction !=null && typeof(json.result) != 'undefined'){
                            callbacks.add(failFunction);
                            callbacks.fire(targetID,json);
                        }else{
                            alert(json.msg);
                            if(typeof(json.fieldname) !='undefined' && json.fieldname != ''){
                                $('#'+json.fieldname).focus();
                            }
                        }
                    }else{
                        callbacks.add(callFunction);
                        callbacks.fire(targetID,json);
                    }
                }
            }
        })
        .fail(function(request, status, error) {
            console.log(status + " " + error + " " + JSON.stringify(request));
            alert(status + " " + error + " " + JSON.stringify(request) );
        })
        .always(function() {
        });
    };
});