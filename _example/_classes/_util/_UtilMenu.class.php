<?php
$path = str_replace($_SERVER['PHP_SELF'],'',__FILE__);
include_once $path.'/config/config.inc.php';

$utilMenu = new UtilMenu(_ROOT_PATH_.'/'._MENU_.'/menu.xml');

# 메뉴 전체
Out::prints_ln('전체메뉴 XML 그대로');
Out::prints_r($utilMenu->getMenus());

# 해당하는 키에 속한 메뉴만
Out::prints_ln('해당하는 키에 속한 메뉴만 : sub_0300');
Out::prints_r($utilMenu->getMenusQuery('sub_0300'));

# 해당키에 속하고 index 순번에 속한 메뉴만
Out::prints_ln('해당키에 속하고 index 순번에 속한 메뉴만 : sub_9900 0번째 배열');
Out::prints_r($utilMenu->getMenusQueryNodes('sub_9900',0));

# 1, 2차 노드 트리 메뉴 만들기
Out::prints_ln('1차, 2차 메뉴');
Out::prints_r($utilMenu->getMenus2Tree('web','sub'));

# 2차, 3차
Out::prints_ln('2차, 3차 메뉴 ');
Out::prints_r($utilMenu->getMenus2Tree('sub_0100','third'));


# 전체 메뉴 트리 구조
Out::prints_ln('전체 메뉴 트리 구조 ');
Out::prints_r($utilMenu->getMenusTree(array('web','sub','third'), 2));
?>