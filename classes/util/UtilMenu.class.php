<?php
/** ======================================================
| @Author   : 김종관 | 010-4023-7046
| @Email    : apmsoft@gmail.com
| @HomePage : http://www.apmsoftax.com
| @Editor   : Eclipse(default)
| @UPDATE   : 2014-12-22
----------------------------------------------------------*/

# res/menu/menu.xml 처리를 위한
# mid 와 menu 노드네임을 원칙으로 한다.
class UtilMenu
{
    var $resObj;
    private $menus = array();
    public $category_node_name = 'mid';

    public function __construct($filename){
        $this->resObj =new Res();
        $this->resObj->setResourceRoot($filename, 'menus');
        $this->menus = $this->resObj->resource->menus;
    }

    #@ array
    # 메뉴 전체를 배열로 리턴
    public function getMenus(){
        return $this->menus;
    }

    #@ array
    # 메뉴 목록중 키에 해당하는 메뉴 배열을 리턴
    public function getMenusQuery($offset){
        $args = array();
        if(self::offsetExists($offset)){
            if(!isset($this->menus[$offset]['menu'][0])){
                $args = array($this->menus[$offset]['menu']);
            }else{
                $args=$this->menus[$offset]['menu'];
            }
        }else{
            //$args = $this->menus[$offset]['menu']=array();
        }
        // print_r($args);
    return $args;
    }

    #@ string
    # 키에 해당하는 또한 index 순번에 해당하는
    public function getMenusQueryNodes($offset, $index){
        $args = self::getMenusQuery($offset);
        return (isset($args[$index])) ? $args[$index] : array();
    }

    #@ array
    #$first_offset, $second_offset
    # 2단 배열의 메뉴 만들기 (1차 2차, 2차 3차)
    public function getMenus2Tree($first_offset, $second_offset)
    {
        $tree_menus=array();
        $tree_menus = self::getMenusQuery($first_offset);
        foreach($tree_menus as $index => $menu)
        {
            $child_offset = $second_offset.'_'.$menu[$this->category_node_name];
            $args=& $tree_menus[$index]['menu'];
            $args=self::getMenusQuery($child_offset);
        }
    return $tree_menus;
    }

    #@ array
    # 전체메뉴 트리구조 array로 출력
    # $offsets = array('web','sub','third') XML 메뉴 키네임
    # $depth_len = 메뉴 문자 깊이 길이
    public function getMenusTree($offsets, $depth_len=2){
        $root_menus = $this->menus;
        $tree_menus=array();
        $tree_menus_reference = array();
        $menu_total_depth = count($offsets);
        foreach($offsets as $index => $offset)
        {
            $parent_num = $index-1;
            // Out::prints_ln($offset.'/'.$parent_num);
            // print_r($root_menus);
            #2차, 3차, 4차 메뉴
            if($index>0){
                foreach($root_menus as $offset2 => $menu){
                    if(strstr($offset2, $offset)!==false)
                    {
                        $menu =&$menu['menu'];
                        $parent_mid = str_replace($offset.'_', '', $offset2);

                        if(isset($tree_menus_reference[$parent_num][$parent_mid]))
                        {
                            $loop2 =&$tree_menus_reference[$parent_num][$parent_mid];
                            // Out::prints_ln('-----------------'.$offset.'/'.$offset2.'/'.$parent_mid);
                            // print_r($menu);
                            if(count($menu))
                            {
                                if(!isset($menu[0])){ $menu=array($menu); }

                                $loop2=$menu;
                                foreach($menu as $k2=>$v2){
                                    $mid = $v2[$this->category_node_name];
                                    $tree_menus_reference[$index][$mid]=&$loop2[$k2]['menu'];
                                    $tree_menus_reference[$index][$mid]=array();
                                }
                            }
                        }
                        unset($root_menus[$offset2]);
                    }
                }
            }
            # 1차메뉴
            else{
                $tree_menus = $root_menus[$offset]['menu'];
                foreach($tree_menus as $k=>$v){
                    $mid = substr($v[$this->category_node_name],0,$depth_len);
                    $mid_repeat = str_repeat("0", ($menu_total_depth*($depth_len-1)));
                    $tree_menus_reference[$index][$mid.$mid_repeat]=&$tree_menus[$k]['menu'];
                    $tree_menus_reference[$index][$mid.$mid_repeat]=array();
                }
                unset($root_menus[$offset]);
            }

        }
        // print_r($tree_menus_reference);
        // print_r($tree_menus);
    return $tree_menus;
    }

    #@ boolean
    # menus에 해당하는 키 밸류값이 있는지 확인
    private function offsetExists($offset){
        if(isset($this->menus[$offset])) return true;
        else return false;
    }
}
?>